require "prawn"
require "prawn/table"
require "arabic-letter-connector"
require "bidi"


# Small use case scenario for Arabic lang support in prawn
# --------------------------------------------------------

# We'll try to evaluate a solution to handle pdf generation
# for an IOS application, where some part writtent in arabic.
file_name = "with_app-name_multiline_description-table.pdf"

# Application of reference 
#
# Urls: 
# https://apps.apple.com/fr/app/%D9%85%D9%86%D8%B5%D8%A9-%D8%B3%D9%87%D9%84/id1492061620
# https://app.apptweak.com/applications/ios/1492061620/metadata?country=sa&device=iphone&language=ar#app-description

app_name = "منصة سهل"
app_description = "هو تطبيق للأجهزة الذكية يخدم طلاب التعليم العام من خلال شروح مرئية للمعلمين والمعلمات عبر (بث مباشر) يتفاعل معها الطلاب والطالبات ."
app_multiline_description = "كما يمكن استعراض الشروح عبر الباركود (QR) المرتبط بكتب المناهج الدراسية بحيث تستطيع الحصول على مجموعة كبيرة من شروح المعلمين والمعلمات لأي فقرة تحتاج إليها في المنهج .


يتضمن هذا التطبيق الميزات التالية:

· تصفح الكتب المدرسية العلمية مع شرح العديد من المعلمين لكل فقرة.

· ميزة التفاعل الباركود (QR) مع الكتب المدرسية.

. حقيبة مفضلة خاصة لكل طالب يمكنه جمع الدروس المهمة الخاصة به.

. البث المباشر في أوقات محددة لشرح بعض الدروس والإجابة على الأسئلة المهمة للطلاب

. إمكانية التعليق والتقييم على شروح المعلمين والمعلمات"



def formated_arabic_lines(lines, pdf, bidi)
    rendered_lines = []
    lines.each_line do |line|
        rendered_line = bidi.render_visual line.connect_arabic_letters
        rendered_lines.push(
            {:text => rendered_line, :direction => :rtl}
        )
        # pdf.text rendered_line, direction: :rtl
    end
    return rendered_lines
end

class PrawnArabicText
    def initialize(bidi)
        @bidi = bidi
    end

    def lines_for_formatted_text(text)
        lines = []
        text.each_line do |line|
            line_connected = self.connected_letters(line)
            lines.push(
                {:text => line_connected, :direction => :rtl}
            )
        end
        return lines
    end

    def connected_letters(text)
        return @bidi.render_visual text.connect_arabic_letters
    end


    def connected_letters_reverse(text)
        return @bidi.render_visual text.connect_arabic_letters.reverse
    end

end


Prawn::Document.generate(file_name) do
    font_families.update(
        "Tahoma" => {
            :normal => "./fonts/Tahoma.ttf",
            :bold => "./fonts/Tagoma+Bold.ttf"
        }
    )
    font "Tahoma"

    pdf = self
    bidi = Bidi.new
    arabicText = PrawnArabicText.new(bidi)

    name_lines = arabicText.lines_for_formatted_text(app_name)
    description_lines = arabicText.lines_for_formatted_text(app_description)
    multi_line_lines = arabicText.lines_for_formatted_text(app_multiline_description)

    formatted_text(name_lines, direction: :rtl)
    formatted_text(description_lines, direction: :rtl)
    formatted_text(multi_line_lines, direction: :rtl)

    #pad_bottom(20){text description, direction: :rtl}
    #render_arabic_text_lines app_multiline_description, self, bidi

    cell_mutliline_description = arabicText.connected_letters_reverse(app_multiline_description)

    move_down(20)

    self.text_direction = :rtl
    row_content = ["", cell_mutliline_description, bidi.render_visual("#{app_name.length} characters")]
    table_content = [row_content]
    table(table_content, column_widths: [35, (pdf.bounds.width - 115), 80], cell_style: { valign: :center, border_width: [1, 0, 1, 0], leading: 0, text_color: "333333", border_color: "dedede", padding: [5, 0, 5, 0], size: 11 })
    self.text_direction = :ltr

    text "keeping my direction"

end